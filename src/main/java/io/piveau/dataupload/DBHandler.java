package io.piveau.dataupload;

import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.BulkOperation;
import io.vertx.ext.mongo.MongoClient;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class DBHandler {

    private Logger log = LoggerFactory.getLogger(getClass());

    private MongoClient mongoClient;
    private String dbName;


    public DBHandler(MongoClient mongoClient, String dbName) {
        this.mongoClient = mongoClient;
        this.dbName = dbName;
    }


    public Future<String> prepareEntry(JsonArray jsonArray) {

        Promise promise = Promise.promise();

        List<BulkOperation> jsonList = new ArrayList<>();
        for (Object object : jsonArray) {
            BulkOperation bulkOperation = BulkOperation.createInsert((JsonObject) object);
            jsonList.add(bulkOperation);
        }

        mongoClient.bulkWrite(dbName, jsonList, res -> {
            if (res.succeeded()) {
                promise.complete("success");
            } else {
                promise.fail(res.cause());
            }
        });

        return promise.future();
    }

    public Future<String> createEntry(byte[] bytes, String filename, String uuid, String token) {
        Promise promise = Promise.promise();

        JsonObject queryDocument = new JsonObject()
                .put("token", token)
                .put("id", uuid);

        mongoClient.find(dbName, queryDocument, res -> {
            if (res.succeeded() && !res.result().isEmpty()) {
                JsonObject document = new JsonObject()
                        .put("id", uuid)
                        .put("binaryData", bytes)
                        .put("fileName", filename);

                deleteEntryByToken(token).setHandler(resRemove -> {
                    if (resRemove.succeeded() && resRemove.result().equals("Deleting complete")) {
                        InsertDocument(document).setHandler(resInsertion -> {
                            if (resInsertion.succeeded()) {
                                promise.complete("success");
                            } else {
                                promise.fail(resInsertion.cause().toString());
                            }
                        });
                    } else {
                        log.info("Failed at delete with cause: " + resRemove.cause());
                        promise.fail(resRemove.cause());
                    }
                });

            } else if (res.result().isEmpty()) {
                promise.fail("entry");
            } else if (res.result() == null) {
                promise.fail("find");
            }
        });

        return promise.future();
    }

    private Future<String> InsertDocument(JsonObject document) {
        Promise promise = Promise.promise();

        mongoClient.insert(dbName, document, resInsertion -> {
            if (resInsertion.succeeded()) {
                promise.complete("success");

            } else {
                promise.fail(resInsertion.cause().toString());
            }

        });
        return promise.future();
    }

    private Future<String> deleteEntryByToken(String token) {
        Promise promise = Promise.promise();

        JsonObject document = new JsonObject()
                .put("token",token);
        mongoClient.removeDocuments(dbName, document, res ->{
            if (res.succeeded()) {
                promise.complete("Deleting complete");
            } else {
                log.info("Deleting failed: " + res.cause());
                promise.fail(res.cause());
            }
        });

        return promise.future();
    }


    public Future<Void> deleteEntryByFileID(String fileID){
        Promise promise = Promise.promise();
        JsonObject document = new JsonObject()
                .put("id",fileID);
        mongoClient.removeDocuments(dbName,document, res ->{
            if (res.succeeded()) {
                res.result();
                promise.complete(res.result().toString());
            } else {
                promise.fail(res.cause());
                promise.fail("Deleting document failed");
            }
        });
        return promise.future();
    }


    public void showAllForOwner(String ownerID, Handler<List> handler){
        JsonObject queryDocument = new JsonObject()
                .put("owner",ownerID);
        mongoClient.find(dbName,queryDocument, res ->{

            if (res.succeeded()) {
                List<JsonObject> result = res.result();
                List<String> returnList= new ArrayList<>();
                for (JsonObject entries : result) {
                    entries.remove("_id");
                    entries.remove("BinaryData");
                }

                handler.handle(res.result());
            } else {
                handler.handle(null);
            }
        });

    }

    public void getFile(String fileID, Handler<File> aHandler){
        JsonObject queryDocument = new JsonObject()
                .put("id",fileID);
        AtomicReference<String> tmpFileName= new AtomicReference<>("");
        mongoClient.find(dbName,queryDocument, res ->{

            if (res.succeeded() && res.result().size()>0) {
                JsonObject returnDocument = res.result().get(0);
                tmpFileName.set("/tmp/" + returnDocument.getString("fileName"));
                try {
                    FileUtils.writeByteArrayToFile(new File(String.valueOf(tmpFileName)), returnDocument.getBinary("binaryData"));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                returnDocument.getBinary("binaryData");
                aHandler.handle(new File(String.valueOf(tmpFileName)));
            } else {
                aHandler.handle(null);
            }
        });

    }


}
